# Changelog

## [v0.2.0](https://gitlab.com/go-utilities/file/-/tags/v0.2.0) (2022-08-27)

### Changed

* renamed functions Exec* -> Run*

## [v0.1.0](https://gitlab.com/go-utilities/file/-/tags/v0.1.0) (2022-08-27)

* Initial version