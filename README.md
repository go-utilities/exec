[![Go Reference](https://pkg.go.dev/badge/gitlab.com/go-utilities/file.svg)](https://pkg.go.dev/gitlab.com/go-utilities/exec)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/go-utilities/file)](https://goreportcard.com/report/gitlab.com/go-utilities/exec)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/go-utilities/file)](https://api.reuse.software/info/gitlab.com/go-utilities/exec)

# Go Utilities: exec

Go package with utilities that support the execution of external commands, extending the functionality of the [standard package os/exec](https://pkg.go.dev/os/exec).
